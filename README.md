# Time Series Database
This project implement a Database targeted for Time Series storage. It try to
achieve the following goals:

 - be horizontally scalable
 - be evolvable over time
 - be accesible over the Web
 - provide multi-tenancy capabilities
 
## Documentation

 - [Concepts](./doc/concepts.md)
 - [Restful API](./doc/restful-api.md)
 - [Configuration](#configuration)
 - [Deployment](#deployment)

## Configuration

The application launcher use [nconf](https://github.com/indexzero/nconf)
library to collect application settings. You can use command line arguments
and environment variables.

### Logging

The application use [Morgan](https://github.com/expressjs/morgan) to log Web 
requests. You can configure this library passing settings into the `morgan`
configuration option.
 
## Deployment
 
This project provide deployment support of the product in the form of an image
for the 
[Docker Container Engine](https://www.docker.com/products/docker-engine).
