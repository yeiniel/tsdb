# RESTful API
The Time Series Database API is implemented using the [REST] architectural 
style. This design decision works toward achieving two of the product goals:
be evolvable over time and be accessible over the Web, increasing 
interoperability on that domain.

    The API described bellow is specific for each tenant.

All resources provide representation using the 
[application/hal+json](http://stateless.co/hal_specification.html) [media type].

## Root Resource
This is the only known entry point into the product API. All the other 
resources are reached using Hypermedia controls.

The root resource is a null one (do not provide any information). It just 
expose relations to other resources that are part of the API.

| Relation | Description |
|----------|-------------|
| rels/has-ts-collection | Point to the [Time Series Collection](#time-series-collection) stored on the Database |

## Time Series Collection

This is the collection of [Time Series](#time-series) stored on the Database.

Each Time Series has the following attributes:

 - the Time Series unique identifier in the context of the Time Series
   Database (id)

New time series can be added to this collection using the POST method. In this
case the [application/json](https://www.ietf.org/rfc/rfc4627.txt) [media type]
can be use for the resource sent on the Web request body. If successful the
response will have an status code of **201** and will include a **Location**
header pointing to the new time series resource. 

## Time Series

This is one of the Time Series stored on the Database.

| Relation | Description |
|----------|-------------|
| rels/has-data | Point to the [collection of Data Points](#time-series-data-collection) for this particular Time Series |

## Time Series Data Collection

This is the collection of Data Points for a particular Time Series. Each data
point has the following attributes:

  - the data point timestamp (t)
  - the data point value (v)

[media type]: https://en.wikipedia.org/wiki/Media_type
[REST]: https://en.wikipedia.org/wiki/Representational_state_transfer