"use strict";

const ava = require("ava");
const nconf = require("nconf");
const supertest = require("supertest");

const historian = require("..");

let agent;
let endpoint;

ava.before(() => {
  const settings = new nconf.Provider()
    .argv()
    .env()
    .get();

  let application = new historian.Application(settings);

  agent = supertest(application.listener);
});

ava.beforeEach(async t => {
  const root = await agent.get("/t/some-tenant");

  if (root.status !== 200) {
    t.fail(root);
  }

  endpoint = root.body._links["rels/has-ts-collection"].href;
});

ava("GET OK", async t => {
  const resource = await agent.get(endpoint);

  t.is(resource.status, 200);
});

ava("GET default media type is application/hal+json", async t => {
  const resource = await agent.get(endpoint);

  t.is(resource.type, "application/hal+json");
});

ava("GET response provide ts collection", async t => {
  const resource = await agent.get(endpoint);

  t.true(Array.isArray(resource.body._embedded.ts));
});

ava("GET implement server side content negotiation", async t => {
  const resource = await agent
    .get(endpoint)
    .set("Accept", "no-acceptable-media-type");

  t.is(resource.status, 406);
});

ava("POST OK", async t => {
  t.plan(3);

  const id = `some-ts-${Math.floor(Math.random() * 10000000)}`;

  const response = await agent.post(endpoint).send({ id });

  t.is(response.status, 201);
  t.is(typeof response.headers["location"], "string");

  const resource = await agent.get(endpoint);

  t.true(resource.body._embedded.ts.find(ts => ts.id === id) !== undefined);
});

ava("POST rejects forcing tenant", async t => {
  const id = `some-ts-${Math.floor(Math.random() * 10000000)}`;

  const response = await agent
    .post(endpoint)
    .send({ id, tenant: "some-other-tenant" });

  t.is(response.status, 500);
});
