"use strict";

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable("ts", {
      tenant: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      }
    }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable("ts")
};
