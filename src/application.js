// @flow

"use strict";

const express = require("express");
const morgan = require("morgan");

const web = require("./web");

const locator = require("./impl/tsdb-locator");

/** Time Series Database
 *
 * This class is used to produce instances of the Time Series Database. It
 * implement the business logic of the application and provide its public API.
 */
class Application extends locator.TimeSeriesDatabaseLocator {
  static get DEFAULT_SETTINGS() /*: any */ {
    return {
      express: JSON.stringify({
        "strict routing": true
      }),
      morgan: "combined"
    };
  }

  /*::
  // cached instance of the Web request event listener
  _listener: express;
  */

  /** Constructor
   *
   * Constructor is override to provide default settings to custom application
   * code.
   * @param settings
   */
  constructor(settings /*: any */) {
    super(Object.assign({}, Application.DEFAULT_SETTINGS, settings));
  }

  /** Web request event listener
   *
   * Handle all Web request events and implement the public application
   * interface as a RESTful API.
   *
   * The Web request event handler creation is delayed until the first time
   * this attribute value is requested.
   */
  get listener() /*: express */ {
    if (!this._listener) {
      this._listener = express();

      // configure the Express.js framework settings
      const settings = JSON.parse(this.settings.express);
      Object.keys(settings).forEach(key =>
        this.listener.set(key, settings[key])
      );

      // use Web request logging middleware
      this._listener.use(morgan(this.settings.morgan));

      // use the Web router provided by this application
      this._listener.use(web.setupRouter(express.Router(), this));
    }

    return this._listener;
  }
}

module.exports = { Application };
