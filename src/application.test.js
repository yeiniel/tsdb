"use strict";

const ava = require("ava");
const supertest = require("supertest");

const { Application } = require("./application");
const web = require("./web");

let locator;

web._setupRouter = web.setupRouter;
web.setupRouter = (router, loc) => {
  locator = loc;
  return web._setupRouter(router, loc);
};

let application /*: Application */;

ava.before(() => {
  application = new Application({
    express: JSON.stringify({
      "some-setting": "some-value"
    })
  });
});

ava("application has `listener` attr", t => t.truthy(application.listener));

ava("application `listener` attr is a function", t =>
  t.true(typeof application.listener === "function")
);

ava("application `listener` attr handle Web request events", async t => {
  const res = await supertest(application.listener).get("/");

  t.true(res.status >= 200 && res.status <= 600);
});

ava("application pass itself as tsdb locator", t => {
  t.is(locator, application);
});

ava("application set express settings using the listener setting", t => {
  t.is(application.listener.get("some-setting"), "some-value");
});
