// @flow

"use strict";

const stream = require("stream");

/*::
const sequelize = require("sequelize");
*/

/** Readable stream of Model instances produced by batching findAll calls */
class Readable extends stream.Readable {
  /*::
  Model: sequelize.Model;

  // options to pass to Model.findAll call on every batch request
  options: any;

  // options to control batch characteristics (size)
  batchOptions: any;
  */

  constructor(
    Model /*: sequelize.Model */,
    options /*: any */ = {},
    batchOptions /*: { limit: number } */ = { limit: 1000 }
  ) {
    super({ objectMode: true });

    this.Model = Model;
    this.options = options;

    // set initial offset to its default if not provided
    if (options.offset === null || options.offset === undefined) {
      options.offset = 0;
    }

    // set initial limit to its default if not provided
    if (options.limit === null || options.limit === undefined) {
      options.limit = Infinity;
    }

    this.batchOptions = Object.assign({}, options, batchOptions);
  }

  _read() {
    // by default in object mode the base class expect a single push per read
    // therefore we need to pause until we push all items
    this.pause();

    this.Model.findAll(this.batchOptions)
      .then(rows => {
        rows.forEach(row => this.push(row));

        // increase batch offset by limit
        this.batchOptions.offset += this.batchOptions.limit;

        // end stream if rows array is empty or less than batch limit or next
        // offset is above limit
        if (
          rows.length === 0 ||
          rows.length < this.batchOptions.limit ||
          this.batchOptions.offset >= this.options.limit
        ) {
          this.push(null);
        }

        // honor limit if set
        if (
          this.batchOptions.offset + this.batchOptions.limit >
          this.options.limit
        ) {
          this.batchOptions.limit =
            this.options.limit - this.batchOptions.offset;
        }

        this.resume();
      })
      .catch(err => {
        this.resume();
        this.emit("error", err);
      });
  }
}

module.exports = { Readable };
