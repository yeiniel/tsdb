/* test suite for impl/findAllStream function */

"use strict";

const ava = require("ava");
const isStream = require("is-stream");

const findAllStream = require("./find-all-stream");

ava("produce readable stream", t => {
  t.true(isStream.readable(new findAllStream.Readable()));
});

ava("read call model findAll", t => {
  let called = false;

  const readable = new findAllStream.Readable({
    findAll: () => {
      called = true;
      return Promise.resolve([]);
    }
  });

  readable.read();

  t.true(called);
});

ava("rows returned by findAll are read", async t => {
  let row;

  const readable = new findAllStream.Readable({
    findAll: () => {
      return Promise.resolve([{ id: "some-unique-id" }]);
    }
  });

  for await (const k of readable) {
    row = k;
  }

  t.is(row.id, "some-unique-id");
});

ava("offset is honored", t => {
  let receivedOffset;
  const offset = Math.random();

  const readable = new findAllStream.Readable(
    {
      findAll: ({ offset }) => {
        receivedOffset = offset;
        return Promise.resolve([]);
      }
    },
    {
      offset
    }
  );

  readable.read();

  t.is(receivedOffset, offset);
});

// TODO: check that limit is honored
