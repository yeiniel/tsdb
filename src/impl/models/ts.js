// @flow

"use strict";

/*::
const sequelize = require("sequelize");
*/

/** Time Series Sequelize ORM model. */
module.exports = (
  sequelize /*: sequelize.Sequelize */,
  DataTypes /*: sequelize.DataTypes */
) =>
  sequelize.define(
    "TimeSeries",
    {
      tenant: {
        type: DataTypes.STRING,
        primaryKey: true
      },
      id: {
        type: DataTypes.STRING,
        primaryKey: true
      }
    },
    {
      tableName: "ts",
      timestamps: false
    }
  );
