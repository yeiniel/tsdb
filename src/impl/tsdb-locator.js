// @flow

"use strict";

const sequelize = require("sequelize");

/*::
const express = require("express");

import {TimeSeriesDatabaseLocator as ITimeSeriesDatabaseLocator} from "../tsdb-locator";
*/

const tsdb = require("./tsdb");

class TimeSeriesDatabaseLocator /*:: implements ITimeSeriesDatabaseLocator */ {
  static get DEFAULT_SETTINGS() /*: any */ {
    return {
      DATABASE_URL: "postgres://user:password@postgres/database"
    };
  }

  /*::
  // locator settings
  settings: any;

  // cached sequelize instance
  _sequelize: ?sequelize.Sequelize;
  */

  constructor(settings /*: any */) {
    this.settings = Object.assign(
      {},
      TimeSeriesDatabaseLocator.DEFAULT_SETTINGS,
      settings
    );
  }

  get sequelize() {
    if (!this._sequelize) {
      this._sequelize = new sequelize.Sequelize(this.settings.DATABASE_URL, {
        operatorsAliases: false
      });

      // import models
      this._sequelize.import("./models/ts");
    }

    return this._sequelize;
  }

  locate(request /*: express.Request */) {
    return Promise.resolve(
      new tsdb.TimeSeriesDatabase(this.sequelize, request.params.tenant)
    );
  }
}

module.exports = { TimeSeriesDatabaseLocator };
