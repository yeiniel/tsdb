/** Test suite for the impl. of the TimeSeriesDatabaseLocator */

"use strict";

const ava = require("ava");

const tsdb = require("./tsdb");
const tsdbLocator = require("./tsdb-locator");

ava("locate() promise TimeSeriesDatabase", async t => {
  const instance = new tsdbLocator.TimeSeriesDatabaseLocator({
    DATABASE_URL: "postgres://user:password@postgres/database"
  });

  const tsdbInstance = await instance.locate({
    params: { tenant: "some-tenant" }
  });

  t.true(tsdbInstance instanceof tsdb.TimeSeriesDatabase);
});

ava("locate() set tenant on tsdb", async t => {
  const instance = new tsdbLocator.TimeSeriesDatabaseLocator({
    DATABASE_URL: "postgres://user:password@postgres/database"
  });

  const tsdbInstance = await instance.locate({
    params: { tenant: "some-tenant" }
  });

  t.is(tsdbInstance.tenant, "some-tenant");
});
