// @flow

"use strict";

/*::
const sequelize = require("sequelize");

import {TimeSeries} from "../ts";
import type {TimeSeriesIdentifier} from "../ts";
import {TimeSeriesDatabase as ITimeSeriesDatabase} from "../tsdb";
*/

const findAllStream = require("./find-all-stream");

class TimeSeriesDatabase /*:: implements ITimeSeriesDatabase */ {
  /*::
  sequelize: sequelize.Sequelize;
  tenant: string;

  // list of model attributes used for cleaning purposes
  modelAttrs: string[];
  */

  constructor(sequelize /*: sequelize.Sequelize */, tenant /*: string */) {
    this.sequelize = sequelize;
    this.tenant = tenant;

    // cache model keys
    this.modelAttrs = Object.keys(sequelize.models.TimeSeries.rawAttributes);
  }

  list() {
    return Promise.resolve(
      new findAllStream.Readable(this.sequelize.models.TimeSeries, {
        where: {
          tenant: this.tenant
        }
      })
    );
  }

  async add(ts /*: TimeSeries */) {
    const tsObj /*: any */ = ts;

    // rejects time series if tenant is forced
    if (tsObj.tenant) {
      throw new Error("Incorrect value for tenant attribute");
    }

    tsObj.tenant = this.tenant;

    return await this.sequelize.models.TimeSeries.create(tsObj);
  }

  get(tsId /*: TimeSeriesIdentifier */) {
    return this.sequelize.models.TimeSeries.findOne({
      where: { tenant: this.tenant, id: tsId }
    });
  }
}

module.exports = { TimeSeriesDatabase };
