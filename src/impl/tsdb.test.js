/** Test suite for impl/TimeSeriesDatabase class */

"use strict";

const ava = require("ava");

const findAllStream = require("./find-all-stream");
const tsdb = require("./tsdb");

ava("list() promise findAllStream readable", async t => {
  const instance = new tsdb.TimeSeriesDatabase(
    {
      models: {
        TimeSeries: {
          rawAttributes: { tenant: null, id: null }
        }
      }
    },
    "some-tenant"
  );

  const readable = await instance.list();

  t.true(readable instanceof findAllStream.Readable);
});

ava("list() set where.tenant on findAllStream readable", async t => {
  const instance = new tsdb.TimeSeriesDatabase(
    {
      models: {
        TimeSeries: {
          rawAttributes: { tenant: null, id: null }
        }
      }
    },
    "some-tenant"
  );

  const readable = await instance.list();

  t.is(readable.options.where.tenant, "some-tenant");
});

ava("add() pass ts to model create", async t => {
  const ts = { id: "some-random-id" };
  let receivedTs = null;

  const instance = new tsdb.TimeSeriesDatabase(
    {
      models: {
        TimeSeries: {
          rawAttributes: { tenant: null, id: null },
          create: function(ts) {
            receivedTs = ts;
            return Promise.resolve("some-value");
          }
        }
      }
    },
    "some-tenant"
  );

  const res = await instance.add(ts);

  t.is(receivedTs, ts);
  t.is(res, "some-value");
});

ava("add() rejects incorrect tenant", async t => {
  const ts = { id: "some-random-id", tenant: "another-tenant" };

  const instance = new tsdb.TimeSeriesDatabase(
    {
      models: {
        TimeSeries: {
          rawAttributes: { tenant: null, id: null },
          create: function() {
            return Promise.resolve("some-value");
          }
        }
      }
    },
    "some-tenant"
  );

  try {
    await instance.add(ts);
  } catch (e) {
    t.is(e.message, "Incorrect value for tenant attribute");
  }
});

ava("add() add tenant to object passed to model.create()", async t => {
  const ts = { id: "some-random-id" };
  let receivedTs = null;

  const instance = new tsdb.TimeSeriesDatabase(
    {
      models: {
        TimeSeries: {
          rawAttributes: { tenant: null, id: null },
          create: function(ts) {
            receivedTs = ts;
            return Promise.resolve("some-value");
          }
        }
      }
    },
    "some-tenant"
  );

  await instance.add(ts);

  t.is(receivedTs.tenant, "some-tenant");
});

ava("get() add tenant to object passed to model.findOne()", async t => {
  const tsId = "some-random-id";
  let receivedTenant = null;

  const instance = new tsdb.TimeSeriesDatabase(
    {
      models: {
        TimeSeries: {
          rawAttributes: { tenant: null, id: null },
          findOne: function({ where: { tenant } }) {
            receivedTenant = tenant;
            return Promise.resolve("some-value");
          }
        }
      }
    },
    "some-tenant"
  );

  await instance.get(tsId);

  t.is(receivedTenant, "some-tenant");
});

ava("get() pass tsId to model findOne", async t => {
  const tsId = "some-random-id";
  let receivedTsId = null;

  const instance = new tsdb.TimeSeriesDatabase(
    {
      models: {
        TimeSeries: {
          rawAttributes: { tenant: null, id: null },
          findOne: function({ where: { id } }) {
            receivedTsId = id;
            return Promise.resolve("some-value");
          }
        }
      }
    },
    "some-tenant"
  );

  const res = await instance.get(tsId);

  t.is(receivedTsId, tsId);
  t.is(res, "some-value");
});
