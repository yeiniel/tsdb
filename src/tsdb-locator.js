// @flow

"use strict";

/*::
const express = require("express");

import {TimeSeriesDatabase} from "./tsdb";

// Locate Time Series Databases based on Web requests
//
// All the Restful API code is tenant agnostic. This interface is used by Web
// controllers to locate Time Series Database based on a Web request.
export interface TimeSeriesDatabaseLocator {
  locate(request: express.Request): Promise<TimeSeriesDatabase>;
}
*/

