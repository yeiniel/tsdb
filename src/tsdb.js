// @flow

"use strict";

/*::
const stream = require("stream");

import {TimeSeries} from "./ts";
import type {TimeSeriesIdentifier} from "./ts";

// Time Series Database
export interface TimeSeriesDatabase {
  // List all Time Series stored on the Database
  //
  // @returns {Promise<stream.Readable<TimeSeries>>} Promise to return a readable stream of Time Series
  list(): Promise<stream.Readable>;

  // Add a new Time Series to the Database
  //
  // @params ts Time Series
  add(ts: TimeSeries): Promise<void>;

  // Get an existing Time Series from the Database
  //
  // @param tsId Time Series Unique Identifier
  // @returns {Promise<TimeSeries>} Promise to return a Time Series
  get(tsId: TimeSeriesIdentifier): Promise<TimeSeries>;
}
*/

