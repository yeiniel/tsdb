// @flow

"use strict";

const tsCollection = require("./ts-collection");
const tsResource = require("./ts");

module.exports = {
  TimeSeriesCollectionController: tsCollection.TimeSeriesCollectionController,
  TimeSeriesResourceController: tsResource.TimeSeriesResourceController
};
