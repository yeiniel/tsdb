/* test suite for the web/controllers package */

"use strict";

const test = require("ava");

const controllers = require(".");

test("package export TimeSeriesCollectionController symbol", t =>
  t.truthy(controllers.TimeSeriesCollectionController));

test("package export TimeSeriesResourceController symbol", t =>
  t.truthy(controllers.TimeSeriesResourceController));
