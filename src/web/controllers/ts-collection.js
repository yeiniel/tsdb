// @flow

"use strict";

const stream = require("stream");
const url = require("url");

/*::
const express = require("express");
*/

const ajv = require("ajv");
const bodyParser = require("body-parser");
const JSONStream = require("JSONStream");
const pump = require("pump");

/*::
import {TimeSeriesDatabaseLocator} from "../../tsdb-locator";
*/

const tsSchema = require("../../ts.schema");

const linkProvider = require("../util/link-provider");

/** Web Controller for the Time Series Collection
 *
 * Provide logic to handle Web requests with GET and POST methods.
 */
class TimeSeriesCollectionController {
  /*::
  tsdbLocator: TimeSeriesDatabaseLocator;

  // input validator engine
  validator: ajv;

  // validate callable for the specific schema of the Time Series
  validate: (any) => boolean;
  */

  constructor(tsdbLocator /*: TimeSeriesDatabaseLocator */) {
    this.tsdbLocator = tsdbLocator;

    this.validator = new ajv({ allErrors: true });

    this.validate = this.validator.compile(tsSchema);
  }

  /** Setup Express.js route to be handle with controller methods
   *
   * @param route {express.Route} The Express.js route to be setup.
   */
  setupRoute(route /*: express.Route */) {
    route
      .get(this.get.bind(this))
      .post(bodyParser.json(), this.post.bind(this));
  }

  /** Handle requests with GET method directed to the ts collection
   *
   * @param request {express.Request} Web request
   * @param response {express.Response} Web response
   * @param next
   * @return {Promise<void>}
   */
  async get(
    request /*: express.Request */,
    response /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    try {
      const tsdb = await this.tsdbLocator.locate(request);

      const tsStream = await tsdb.list();

      response.format({
        "application/hal+json": () => {
          response.setHeader("Connection", "Transfer-Encoding");
          response.setHeader("Transfer-Encoding", "chunked");

          pump(
            tsStream,
            // transform Sequelize model instances into plain objects
            new stream.Transform({
              objectMode: true,
              transform: (instance, _, done) => done(null, instance.toJSON())
            }),
            // provide hal+json links
            new linkProvider.LinkProvider({
              self: ts => ({
                href: url.resolve(request.originalUrl, `./ts/${ts.id}`)
              }),
              "has-data": ts => ({
                href: url.resolve(request.originalUrl, `./ts/${ts.id}/data`)
              })
            }),
            JSONStream.stringify('{ "_embedded": { "ts": [', ",", "]}}"),
            response,
            err => {
              if (err) {
                next(err);
              }
            }
          );
        }
      });
    } catch (e) {
      next(e);
    }
  }

  /** Handle requests with POST method directed to the ts collection
   *
   * This Web request handler implement adding a new Time Series to the Time
   * Series Database.
   *
   * @param request {express.Request} Web request
   * @param response {express.Response} Web response
   * @param next
   * @return {Promise<void>}
   */
  async post(
    request /*: express.Request */,
    response /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    // perform basic content negotiation check to avoid invoke de backend
    // unnecessary
    if (!request.is("application/json")) {
      response.status(415);
      return next(new Error("Unsupported Media Type"));
    }

    // validate input
    if (!this.validate(request.body)) {
      response.status(422);
      return next(this.validator.errorsText(this.validate.errors));
    }

    const tsdb = await this.tsdbLocator.locate(request);

    try {
      await tsdb.add(request.body);
    } catch (e) {
      return next(e);
    }

    response
      .status(201)
      .location(url.resolve(request.originalUrl, `./ts/${request.body.id}`))
      .end();
  }
}

module.exports = { TimeSeriesCollectionController };
