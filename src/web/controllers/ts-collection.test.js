/* test suite for the web controller for the Time Series collection */

"use strict";

const stream = require("stream");

const ava = require("ava");
const express = require("express");
const supertest = require("supertest");

const { TimeSeriesCollectionController } = require("./ts-collection");

function locate(request) {
  return Promise.resolve({
    list: () =>
      Promise.resolve(
        new stream.Readable({
          objectMode: true,
          read: function() {
            // provide some sample data
            this.push({
              toJSON: () => ({
                tenant: request.params.tenant,
                id: "some-unique-id"
              })
            });
            this.push(null);
          }
        })
      ),
    add: () => Promise.resolve()
  });
}

let listener;
let agent;
let endpoint;

ava.before(() => {
  listener = express();

  new TimeSeriesCollectionController({ locate }).setupRoute(
    listener.route("/:tenant")
  );

  agent = supertest(listener);

  endpoint = "/some-tenant";
});

ava("handle GET method", async t => {
  const res = await agent.get(endpoint);

  t.is(res.status, 200);
});

ava("GET default media type is application/hal+json", async t => {
  const res = await agent.get(endpoint);

  t.is(res.type, "application/hal+json");
});

ava("GET response provide ts collection", async t => {
  const res = await agent.get(endpoint);

  t.true(Array.isArray(res.body._embedded.ts));
  t.is(res.body._embedded.ts.length, 1);
  t.is(res.body._embedded.ts[0].tenant, "some-tenant");
});

ava("GET collection items has self links", async t => {
  const res = await agent.get(endpoint);

  t.true(res.body._embedded.ts.every(ts => ts._links.self));
});

ava("GET collection items has has-data links", async t => {
  const res = await agent.get(endpoint);

  t.true(res.body._embedded.ts.every(ts => ts._links["has-data"]));
});

ava("GET implement server side content negotiation", async t => {
  const res = await agent
    .get(endpoint)
    .set("Accept", "no-acceptable-media-type");

  t.is(res.status, 406);
});

ava("handle POST method", async t => {
  const res = await agent.post(endpoint).send({ id: "some-unique-id" });

  t.is(res.status, 201);
});

ava("POST include location header", async t => {
  const res = await agent.post(endpoint).send({ id: "some-unique-id" });

  t.truthy(res.headers.location);
});

ava("POST validate request data", async t => {
  const res = await agent
    .post(endpoint)
    .send({ another_attr: "some-unique-id" });

  t.is(res.status, 422);
});

ava("POST implement server side content negotiation", async t => {
  const res = await agent
    .post(endpoint)
    .send(JSON.stringify({ id: "some-unique-id" }))
    .set("Content-Type", "no-acceptable-media-type");

  t.is(res.status, 415);
});
