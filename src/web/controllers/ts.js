// @flow

"use strict";

const stream = require("stream");
const url = require("url");

/*::
const express = require("express");
*/

const JSONStream = require("JSONStream");
const pump = require("pump");

/*::
import {TimeSeriesDatabaseLocator} from "../../tsdb-locator";
*/

const linkProvider = require("../util/link-provider");

/** Web Controller for the Time Series Collection
 *
 * Provide logic to handle Web requests with GET and POST methods.
 */
class TimeSeriesResourceController {
  /*::
  tsdbLocator: TimeSeriesDatabaseLocator;
  */

  constructor(tsdbLocator /*: TimeSeriesDatabaseLocator */) {
    this.tsdbLocator = tsdbLocator;
  }

  /** Setup Express.js route to be handle with controller methods
   *
   * @param route {express.Route} The Express.js route to be setup.
   */
  setupRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
  }

  /** Handle requests with GET method directed to the ts resource
   *
   * @param request {express.Request} Web request
   * @param response {express.Response} Web response
   * @param next
   * @return {Promise<void>}
   */
  async get(
    request /*: express.Request */,
    response /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    try {
      const tsdb = await this.tsdbLocator.locate(request);

      const ts = await tsdb.get(request.params.ts);

      response.format({
        "application/hal+json": () => {
          response.setHeader("Connection", "Transfer-Encoding");
          response.setHeader("Transfer-Encoding", "chunked");

          const tsStream = new stream.Transform({
            objectMode: true,
            transform: (instance, _, done) => done(null, instance.toJSON())
          });

          pump(
            tsStream,
            // provide hal+json links
            new linkProvider.LinkProvider({
              self: ts => ({
                href: url.resolve(request.originalUrl, `./ts/${ts.id}`)
              }),
              "has-data": ts => ({
                href: url.resolve(request.originalUrl, `./ts/${ts.id}/data`)
              })
            }),
            JSONStream.stringify("", "", ""),
            response,
            err => {
              if (err) {
                next(err);
              }
            }
          );

          // $FlowFixMe
          tsStream.write(ts);
          tsStream.end();
        }
      });
    } catch (e) {
      next(e);
    }
  }
}

module.exports = { TimeSeriesResourceController };
