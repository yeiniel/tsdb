/* test suite for the web controller for the Time Series resource */

"use strict";

const stream = require("stream");

const ava = require("ava");
const express = require("express");
const supertest = require("supertest");

const { TimeSeriesResourceController } = require("./ts");

function locate(request) {
  return Promise.resolve({
    get: tsId =>
      Promise.resolve({
        toJSON: () => ({
          tenant: request.params.tenant,
          id: tsId
        })
      })
  });
}

let listener;
let agent;
let endpoint;

ava.before(() => {
  listener = express();

  new TimeSeriesResourceController({ locate }).setupRoute(
    listener.route("/:tenant/:ts")
  );

  agent = supertest(listener);

  endpoint = "/some-tenant/some-ts-id";
});

ava("handle GET method", async t => {
  const res = await agent.get(endpoint);

  t.is(res.status, 200);
});

ava("GET default media type is application/hal+json", async t => {
  const res = await agent.get(endpoint);

  t.is(res.type, "application/hal+json");
});

ava("GET response provide ts resource", async t => {
  const res = await agent.get(endpoint);

  t.truthy(res.body.id);
});

ava("GET resource has self links", async t => {
  const res = await agent.get(endpoint);

  t.truthy(res.body._links.self);
});

ava("GET collection items has has-data links", async t => {
  const res = await agent.get(endpoint);

  t.truthy(res.body._links["has-data"]);
});

ava("GET implement server side content negotiation", async t => {
  const res = await agent
    .get(endpoint)
    .set("Accept", "no-acceptable-media-type");

  t.is(res.status, 406);
});
