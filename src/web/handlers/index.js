// @flow

"use strict";

const tenant = require("./tenant");

module.exports = {
  tenantHandler: tenant.handler
};
