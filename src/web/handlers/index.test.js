/* test suite for the web/handlers package */

"use strict";

const test = require("ava");

const handlers = require(".");

test("package export tenantHandler symbol", t =>
  t.truthy(handlers.tenantHandler));
