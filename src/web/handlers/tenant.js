// @flow

"use strict";

/*::
const express = require("express");
*/

const url = require("url");

/** Handle Web requests with GET method for the root resource */
function handler(req /*: express.Request */, res /*: express.Response */) {
  res.format({
    "application/hal+json": () => {
      // res.json() can't be used because it override the response content type

      res.write(
        JSON.stringify({
          _links: {
            self: {
              href: req.originalUrl
            },
            "rels/has-ts-collection": {
              href: url.resolve(req.originalUrl, `./${req.params.tenant}/ts`)
            }
          }
        })
      );
      res.end();
    }
  });
}

module.exports = { handler };
