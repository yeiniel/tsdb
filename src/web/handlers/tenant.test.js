/* test suite for the web handler for the root resource of the API */

"use strict";

const ava = require("ava");
const express = require("express");
const supertest = require("supertest");

const { handler } = require("./tenant");

let listener;

ava.before(() => {
  listener = express();

  listener.get("/", handler);
});

ava("handle GET method", async t => {
  const res = await supertest(listener).get("/");

  t.is(res.status, 200);
});

ava("default media type is application/hal+json", async t => {
  const res = await supertest(listener).get("/");

  t.is(res.type, "application/hal+json");
});

ava("response include rels/has-ts-collection relation", async t => {
  const res = await supertest(listener).get("/");

  t.truthy(res.body._links["rels/has-ts-collection"].href);
});

ava("implement server side content negotiation", async t => {
  const res = await supertest(listener)
    .get("/")
    .set("Accept", "no-acceptable-media-type");

  t.is(res.status, 406);
});
