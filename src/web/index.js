// @flow

"use strict";

/*::
const express = require("express");

import {TimeSeriesDatabaseLocator} from "../tsdb-locator";
*/

const controllers = require("./controllers");
const handlers = require("./handlers");

/** Setup Web router to implement the RESTful API */
function setupRouter(
  router /*: express.Router */,
  locator /*: TimeSeriesDatabaseLocator */
) /*: express.Router */ {
  // route requests for the root resource
  router.get("/t/:tenant", handlers.tenantHandler);

  // route requests for the time series collection of a specific tenant
  new controllers.TimeSeriesCollectionController(locator).setupRoute(
    router.route("/t/:tenant/ts")
  );

  // route requests for a specific time series resource of a specific tenant
  new controllers.TimeSeriesResourceController(locator).setupRoute(
    router.route("/t/:tenant/ts/:ts")
  );

  return router;
}

module.exports = { setupRouter };
