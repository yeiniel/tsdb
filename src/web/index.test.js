/* test suite for the web package */

"use strict";

const ava = require("ava");
const express = require("express");

let tsCollectionLocator;

class TimeSeriesCollectionController {
  constructor(loc) {
    tsCollectionLocator = loc;
  }

  setupRoute() {}
}

let tsResourceLocator;

class TimeSeriesResourceController {
  constructor(loc) {
    tsResourceLocator = loc;
  }

  setupRoute() {}
}

const controllers = require("./controllers");

controllers.TimeSeriesCollectionController = TimeSeriesCollectionController;
controllers.TimeSeriesResourceController = TimeSeriesResourceController;

const { setupRouter } = require(".");

ava("return the same router", t => {
  const router = express.Router();

  t.is(setupRouter(router), router);
});

ava("setup route for the tenant resource", t => {
  const router = express.Router();
  let routeRoot = false;
  router.route = path => {
    if (path === "/t/:tenant") {
      routeRoot = true;
    }

    return { get: () => null };
  };

  setupRouter(router);

  t.true(routeRoot);
});

ava("setup route for the time series collection", t => {
  const router = express.Router();
  let routeTs = false;
  router.route = path => {
    if (path === "/t/:tenant/ts") {
      routeTs = true;
    }

    return { get: () => null };
  };

  setupRouter(router);

  t.true(routeTs);
});

ava("pass locator to the TimeSeriesCollectionController constructor", t => {
  const locator = `some-locator-${Math.random()}`;
  const router = express.Router();
  router.route = () => {
    return { get: () => null };
  };

  setupRouter(router, locator);

  t.is(tsCollectionLocator, locator);
});

ava("setup route for the time series resource", t => {
  const router = express.Router();
  let routeTs = false;
  router.route = path => {
    if (path === "/t/:tenant/ts/:ts") {
      routeTs = true;
    }

    return { get: () => null };
  };

  setupRouter(router);

  t.true(routeTs);
});

ava("pass locator to the TimeSeriesResourceController constructor", t => {
  const locator = `some-locator-${Math.random()}`;
  const router = express.Router();
  router.route = () => {
    return { get: () => null };
  };

  setupRouter(router, locator);

  t.is(tsResourceLocator, locator);
});
