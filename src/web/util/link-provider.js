// @flow

"use strict";

const stream = require("stream");

/** Provide links to objects
 *
 * This class implement a stream transformation that provide hal+json links
 * to streamed objects.
 */
class LinkProvider extends stream.Transform {
  /*::
  relations: { [string]: function };
  */

  constructor(relations /*: { [string]: function } */) {
    // validate input
    if (
      Object.keys(relations) === 0 ||
      !Object.keys(relations)
        .map(k => relations[k])
        .every(ro => typeof ro === "function")
    ) {
      throw new Error("relations is not a dictionary of mapping functions");
    }

    super({ objectMode: true });

    this.relations = relations;
  }

  _transform(object /*: any */, _ /*: any */, done /*: function */) {
    // add _links attribute if missing
    object._links = object._links || {};

    try {
      // add or replace link relations
      for (let key in this.relations) {
        object._links[key] = this.relations[key].call(object, object);
      }
    } catch (e) {
      this.emit("error", e);
    }

    done(null, object);
  }
}

module.exports = { LinkProvider };
