/* test suite for the link provider web utility */

"use strict";

const ava = require("ava");

const linkProvider = require("./link-provider");

ava("constructor require non-empty dict", t => {
  t.throws(() => new linkProvider.LinkProvider());
});

ava("add link", t => {
  const transform = new linkProvider.LinkProvider({
    self: o => ({ href: "/some-test" })
  });

  const object = { attr: "value" };

  transform.write(object);

  const read = transform.read();

  t.is(object, read);
  t.truthy(read._links.self.href);
});
